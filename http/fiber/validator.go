package fiber

import (
	"reflect"
	"strings"

	"github.com/gofiber/fiber/v2"

	"github.com/go-playground/validator/v10"
)

//RequestHandler handles and validates incoming requests
func ValidateRequest(ctx *fiber.Ctx, v interface{}) (bool, error) {

	if err := ctx.BodyParser(v); err != nil {
		return false, err
	}

	if errs := Validate(v); errs != nil {
		invalidParams := map[string]interface{}{
			"invalid-params": errs,
		}

		NewProblemRequestValidationFailed(ctx, invalidParams)
		return false, nil
	}
	return true, nil
}

//ErrorResponse defines the a validation error structure
type ErrorResponse struct {
	FailedField string `json:"field"`
	Tag         string `json:"validation"`
	Value       string `json:"validation_value"`
}

//Validate validates a struct based on the tags provided in the struct
func Validate(v interface{}) []*ErrorResponse {
	var errors []*ErrorResponse

	validate := validator.New()

	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	err := validate.Struct(v)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ErrorResponse
			element.FailedField = strings.Join(strings.Split(err.Namespace(), ".")[1:], ".")
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}

	return errors
}
