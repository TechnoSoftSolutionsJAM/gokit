package fiber

import "github.com/gofiber/fiber/v2"

//RFC 7807 - Problem details definition
type ProblemDetailStatus int

var (
	ProblemRequestValidationFailed ProblemDetailStatus = 0
	ProblemMissingOrMalformToken   ProblemDetailStatus = 1
	ProblemInvalidOrExpiredToken   ProblemDetailStatus = 2
	ProblemUnauthorized            ProblemDetailStatus = 3
	ProblemConflict                ProblemDetailStatus = 4
	ProblemNotFound                ProblemDetailStatus = 5
	ProblemInternalServerError     ProblemDetailStatus = 6
)

var problemDetails map[ProblemDetailStatus]fiber.Map

func init() {
	problemDetails = make(map[ProblemDetailStatus]fiber.Map)

	problemDetails[ProblemRequestValidationFailed] = fiber.Map{
		"type":   "To be implemented",
		"status": fiber.ErrUnprocessableEntity.Code,
		"title":  "Your request parameters didn't validate",
	}

	problemDetails[ProblemMissingOrMalformToken] = fiber.Map{
		"type":   "To be implemented",
		"status": fiber.StatusBadRequest,
		"title":  "Missing or malformed JWT",
	}

	problemDetails[ProblemInvalidOrExpiredToken] = fiber.Map{
		"type":   "To be implemented",
		"status": fiber.StatusUnauthorized,
		"title":  "Invalid or expired JWT",
	}

	problemDetails[ProblemConflict] = fiber.Map{
		"type":   "To be implemented",
		"status": fiber.StatusConflict,
		"title":  "Resource already exist",
	}

	problemDetails[ProblemUnauthorized] = fiber.Map{
		"type":   "To be implemented",
		"status": fiber.StatusUnauthorized,
		"title":  "Resource already exist",
	}
}

func NewProblemRequestValidationFailed(ctx *fiber.Ctx, keys ...map[string]interface{}) error {
	for _, key := range keys {
		for k, v := range key {
			problemDetails[ProblemRequestValidationFailed][k] = v
		}
	}

	return ctx.Status(fiber.StatusUnprocessableEntity).JSON(problemDetails[ProblemRequestValidationFailed])
}

func NewProblemMissingOrMalformToken(ctx *fiber.Ctx) error {
	return ctx.Status(fiber.StatusBadRequest).JSON(problemDetails[ProblemMissingOrMalformToken])
}

func NewProblemInvalidOrExpiredToken(ctx *fiber.Ctx) error {
	return ctx.Status(fiber.StatusUnauthorized).JSON(problemDetails[ProblemInvalidOrExpiredToken])
}

func NewProblemUnauthorized(ctx *fiber.Ctx, title string) error {
	if title != "" {
		problemDetails[ProblemUnauthorized]["title"] = title
	}

	return ctx.Status(fiber.StatusUnauthorized).JSON(problemDetails[ProblemUnauthorized])
}

func NewProblemConflict(ctx *fiber.Ctx, title string, keys ...map[string]interface{}) error {
	for _, key := range keys {
		for k, v := range key {
			problemDetails[ProblemRequestValidationFailed][k] = v
		}
	}

	if title != "" {
		problemDetails[ProblemRequestValidationFailed]["title"] = title
	}

	return ctx.Status(fiber.StatusConflict).JSON(problemDetails[ProblemConflict])
}

func NewProblemNotFound(ctx *fiber.Ctx, title string) error {
	if title != "" {
		problemDetails[ProblemNotFound]["title"] = title
	}

	return ctx.Status(fiber.StatusConflict).JSON(problemDetails[ProblemNotFound])
}

func NewProblemInternalServerError(ctx *fiber.Ctx, keys ...map[string]interface{}) error {
	for _, key := range keys {
		for k, v := range key {
			problemDetails[ProblemRequestValidationFailed][k] = v
		}
	}

	return ctx.Status(fiber.StatusInternalServerError).JSON(problemDetails[ProblemInternalServerError])
}
