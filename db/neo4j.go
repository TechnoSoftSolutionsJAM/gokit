package db

import (
	"fmt"
	"log"
	"time"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

//ConnectToNeo4jDBWithDefaults connects to a neo4j with default configuration
func ConnectToNeo4jDBWithDefaultOptions(host, port, username, password string) neo4j.Driver {
	configForNeo4j := func(conf *neo4j.Config) {
		conf.MaxTransactionRetryTime = 30 * time.Second
		conf.MaxConnectionPoolSize = 50
		conf.ConnectionAcquisitionTimeout = 2 * time.Minute
		conf.Encrypted = false
	}

	connString := fmt.Sprintf("bolt://%s:%s", host, port)
	driver, err := neo4j.NewDriver(connString, neo4j.BasicAuth(username, password, ""), configForNeo4j)
	if err != nil {
		log.Panicf("[critical error]: %v", err)
	}

	return driver
}

//ConnectToNeo4jDB connects to a neo4j provided a neo4j config
func ConnectToNeo4jDB(host, port, username, password string, cfgFunc func(cfg *neo4j.Config)) neo4j.Driver {
	connString := fmt.Sprintf("bolt://%s:%s", host, port)
	driver, err := neo4j.NewDriver(connString, neo4j.BasicAuth(username, password, ""), cfgFunc)
	if err != nil {
		log.Panicf("[critical error]: %v", err)
	}

	return driver
}
