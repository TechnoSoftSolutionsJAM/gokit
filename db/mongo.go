package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//ConnectToMongoDB provide a client to a mongo db database
//using the provided connection string
func ConnectToMongoDBWithDefaultsOptions(ctx context.Context, connectionStr string) *mongo.Client {
	client, err := mongo.NewClient(options.Client().ApplyURI(connectionStr))
	if err != nil {
		log.Panicf("[critical error]: %v", err)
	}

	err = client.Connect(ctx)
	if err != nil {
		log.Fatalf("[critical error]: %v", err)
	}

	return client
}
