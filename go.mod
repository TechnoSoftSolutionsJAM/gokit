module bitbucket.org/TechnoSoftSolutionsJAM/gokit

go 1.16

require (
	github.com/go-playground/validator/v10 v10.5.0
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/json-iterator/go v1.1.10
	github.com/mergermarket/go-pkcs7 v0.0.0-20170926155232-153b18ea13c9
	github.com/neo4j/neo4j-go-driver v1.8.3
	github.com/sirupsen/logrus v1.8.1
	go.mongodb.org/mongo-driver v1.5.1
	gopkg.in/yaml.v2 v2.4.0
)
