package security

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io"

	"github.com/mergermarket/go-pkcs7"
)

var globalKey []byte

//NewAES256Key sets the global key
//to encrypt and decrypt AES256 cipher text
func NewAES256Key(key []byte) {
	globalKey = key
}

//Encrypt encrypts plain text string into cipher text string
func EncryptUsingAES256(unencrypted string) (string, error) {
	plainText := []byte(unencrypted)
	plainText, err := pkcs7.Pad(plainText, aes.BlockSize)
	if err != nil {
		return "", fmt.Errorf(`plainText: "%s" has error`, plainText)
	}
	if len(plainText)%aes.BlockSize != 0 {
		err := fmt.Errorf(`plainText: "%s" has the wrong block size`, plainText)
		return "", err
	}

	block, err := aes.NewCipher(globalKey)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainText)

	return fmt.Sprintf("%x", cipherText), nil
}

//Decrypt decrypts cipher text string into plain text string
func DecryptUsingAES256(encrypted string) (string, error) {
	cipherText, _ := hex.DecodeString(encrypted)
	block, err := aes.NewCipher(globalKey)
	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		err := fmt.Errorf("cipherText too short")
		return "", err
	}

	iv := globalKey[:aes.BlockSize]
	if len(cipherText) > 16 {
		iv = cipherText[:aes.BlockSize]
		cipherText = cipherText[aes.BlockSize:]
	}

	if len(cipherText)%aes.BlockSize != 0 {
		err := fmt.Errorf("cipherText is not a multiple of the block size")
		return "", err
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)

	cipherText, _ = pkcs7.Unpad(cipherText, aes.BlockSize)

	return fmt.Sprintf("%s", cipherText), nil
}

//Encrypt encrypts plain text string into cipher text string
//provided a key
func EncryptUsingAES256WithKey(unencrypted string, key []byte) (string, error) {
	plainText := []byte(unencrypted)
	plainText, err := pkcs7.Pad(plainText, aes.BlockSize)
	if err != nil {
		return "", fmt.Errorf(`plainText: "%s" has error`, plainText)
	}
	if len(plainText)%aes.BlockSize != 0 {
		err := fmt.Errorf(`plainText: "%s" has the wrong block size`, plainText)
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(plainText))
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainText)

	return fmt.Sprintf("%x", cipherText), nil
}

//Decrypt decrypts cipher text string into plain text string
//provided a key
func DecryptUsingAES256WithKey(encrypted string, key []byte) (string, error) {
	cipherText, _ := hex.DecodeString(encrypted)
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	if len(cipherText) < aes.BlockSize {
		err := fmt.Errorf("cipherText too short")
		return "", err
	}

	iv := globalKey[:aes.BlockSize]
	if len(cipherText) > 16 {
		iv = cipherText[:aes.BlockSize]
		cipherText = cipherText[aes.BlockSize:]
	}

	if len(cipherText)%aes.BlockSize != 0 {
		err := fmt.Errorf("cipherText is not a multiple of the block size")
		return "", err
	}

	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)

	cipherText, _ = pkcs7.Unpad(cipherText, aes.BlockSize)

	return fmt.Sprintf("%s", cipherText), nil
}
