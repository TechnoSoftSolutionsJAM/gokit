package security

import (
	"fmt"
	"reflect"
)

var parser func(v interface{}, p func(u string) (string, error)) error

func init() {
	parser = func(v interface{}, p func(u string) (string, error)) error {
		if v == nil {
			return nil
		}

		val := reflect.ValueOf(v)

		//Checks if initial value is a struct or pointer to a struct
		if val.Kind() == reflect.Ptr && val.Elem().Kind() == reflect.Struct {
			val = val.Elem()
		} else {
			return fmt.Errorf("v must case be a struct")
		}

		numOfFields := val.NumField()

		for i := 0; i < numOfFields; i++ {
			field := val.Field(i)
			fieldKind := field.Kind()

			// Check if it's a pointer to a struct.
			if fieldKind == reflect.Ptr && field.Elem().Kind() == reflect.Struct && field.CanInterface() {
				return parser(field.Interface(), p)
			}

			//Check if struct
			if fieldKind == reflect.Struct && field.CanAddr() && field.Addr().CanInterface() {
				parser(field.Addr().Interface(), p)
				continue
			}

			//Check if string or pointer to string
			if fieldKind == reflect.String || (fieldKind == reflect.Ptr && field.Elem().Kind() == reflect.String) {
				fieldType := val.Type().Field(i)

				if _, ok := fieldType.Tag.Lookup("aes"); !ok {
					continue
				}

				if field.CanSet() {
					if field.String() != "" {
						val, err := p(field.String())
						if err != nil {
							return err
						}
						field.SetString(val)
					}
				}

			}
		}
		return nil
	}
}

//EncryptViaTags encrypt struct fields with the tag "aes"
func EncryptViaTags(v interface{}) error {
	return parser(v, EncryptUsingAES256)
}

//DecryptViaTags decrypt struct fields with the tag "aes"
func DecryptViaTags(v interface{}) error {
	return parser(v, DecryptUsingAES256)
}
