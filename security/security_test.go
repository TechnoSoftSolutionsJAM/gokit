package security

import (
	"fmt"
	"testing"
)

var key []byte = []byte("bPeShVmYq3t6w9z$C&F)J@McQfTjWnZr")

func TestEncryption(t *testing.T) {
	//data to encrypt
	plainText := "zeus"

	//Set AES256 key
	NewAES256Key(key)

	//Encrypt data
	c, err := EncryptUsingAES256(plainText)
	if err != nil {
		fmt.Printf("Unable to encrypt data: %v", err)
		t.FailNow()
	}

	if c == "" || c == plainText {
		fmt.Printf("Unable to encrypt plain text, got: %s", c)
		t.FailNow()
	}

	d, err := DecryptUsingAES256(c)
	if err != nil {
		fmt.Printf("Unable to decrypt data: %v", err)
		t.FailNow()
	}

	if d != plainText {
		fmt.Printf("Unable to decrypt cipher text, expected: %s, got: %s", plainText, d)
		t.FailNow()
	}
}

func TestEncrypViaTags(t *testing.T) {
	//Test data
	p := struct {
		FirstName string `aes:""`
		LastName  string `aes:""`
		Employer  struct {
			Name string `aes:""`
		}
	}{
		FirstName: "yanik",
		LastName:  "",
		Employer: struct {
			Name string `aes:""`
		}{
			Name: "Technosoft",
		},
	}

	dataBeforeEncryption := p

	//Set AES256 key
	NewAES256Key(key)

	//Encrypt data
	err := EncryptViaTags(&p)
	if err != nil {
		fmt.Printf("Unable to encrypt data: %v", err)
		t.FailNow()
	}

	//Check if data was encrypted
	if (p.FirstName == dataBeforeEncryption.FirstName && p.FirstName != "") ||
		(p.LastName == dataBeforeEncryption.LastName && p.LastName != "") {
		fmt.Printf("First name or last name was not encrypted, got: %+v", p)
		t.FailNow()
	}

	if p.Employer.Name == dataBeforeEncryption.Employer.Name && p.Employer.Name != "" {
		fmt.Printf("Employer name was not encrypted, got: %+v", p.Employer)
		t.FailNow()
	}

	//Decrypt data
	err = DecryptViaTags(&p)
	if err != nil {
		fmt.Printf("Unable to decrypt data: %v", err)
		t.FailNow()
	}

	//Check if data was decrypted
	if p.FirstName != dataBeforeEncryption.FirstName ||
		p.LastName != dataBeforeEncryption.LastName {
		fmt.Println("First name or last name was not decrypted")
		t.FailNow()
	}

	if p.Employer.Name != dataBeforeEncryption.Employer.Name {
		fmt.Println("Employer name was not decrypted")
		t.FailNow()
	}
}
