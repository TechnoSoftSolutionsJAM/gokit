package config

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

func InitConfig(path string) error {

	//Open config file
	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("[error] unable to open config file: %w", err)
	}
	defer file.Close()

	fileContent, err := ioutil.ReadAll(file)
	if err != nil {
		return fmt.Errorf("[error] unable to read config file: %w", err)
	}

	err = yaml.Unmarshal([]byte(fileContent), &YAMLConfig)
	if err != nil {
		return fmt.Errorf("[error] unable to parse yaml file: %w", err)
	}

	//Load Public and Private keys into memory
	if len(strings.Trim(YAMLConfig.Path.PrivateKey, "")) > 0 {
		b, err := ioutil.ReadFile(YAMLConfig.Path.PrivateKey)
		if err != nil {
			return fmt.Errorf("[error] unable to open public key file: %w", err)
		}

		block, _ := pem.Decode(b)
		privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return fmt.Errorf("[error] invalid key: %w", err)
		}

		YAMLConfig.Keys.PrivateKey = privateKey
	}

	//Load AES key into memory
	if len(strings.Trim(YAMLConfig.Path.AESKey, "")) > 0 {
		b, err := ioutil.ReadFile(YAMLConfig.Path.AESKey)
		if err != nil {
			return fmt.Errorf("[error] unable to open public key file: %w", err)

		}
		YAMLConfig.Keys.AESKey = b
	}

	return nil
}

//A Config defines the structure of the configuration file
type Configuration struct {
	Server struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
	} `yaml:"server"`

	Database struct {
		Neo4j struct {
			Host     string `yaml:"host"`
			Port     string `yaml:"port"`
			User     string `yaml:"user"`
			Password string `yaml:"password"`
		} `yaml:"neo4j"`
		Mongo struct {
			URI string `yaml:"uri"`
		}
	} `yaml:"database"`

	BatchInsert struct {
		BatchSize    int `yaml:"batchSize"`
		CSVStartLine int `yaml:"csvStartLine"`
	} `yaml:"batchInsert"`

	SMTP struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		User     string `yaml:"username"`
		Password string `yaml:"password"`
	} `yaml:"smtp"`

	Path struct {
		Log        string `yaml:"log"`
		AESKey     string `yaml:"aesKey"`
		PrivateKey string `yaml:"privateKey"`
		APIKeyPath string `yaml:"apiKeyPath"`
	} `yaml:"path"`

	Keys struct {
		AESKey     []byte
		PrivateKey *rsa.PrivateKey
	}

	ValidHosts []string `yaml:"validHosts"`

	Directories struct {
		Uploads string `yaml:"uploads"`
	} `yaml:"directories"`
}

var YAMLConfig *Configuration
