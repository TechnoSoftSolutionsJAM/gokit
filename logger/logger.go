package logger

import (
	"os"

	log "github.com/sirupsen/logrus"
)

func InitLogger(filename string) {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Println("[error] creating log file...", err)
	}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(file)
}
