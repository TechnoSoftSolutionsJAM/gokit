package formatter

import (
	"reflect"
	"strconv"
	"strings"
	"time"

	jsoniter "github.com/json-iterator/go"
)

//ConvertStructToMap takes a struct and converts
// it to a map using the json tags
func ConvertStructToMap(v interface{}) ([]map[string]interface{}, error) {
	var params []map[string]interface{}
	if reflect.ValueOf(v).Kind() == reflect.Slice {
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		result, err := json.Marshal(&v)
		if err != nil {
			return nil, err
		}
		if err = json.Unmarshal(result, &params); err != nil {
			return nil, err
		}
	}

	return params, nil
}

//ConvertToStructOrMap takes a struct to map and visa versa using the json tags
func ConvertToStructOrMap(src interface{}, dst interface{}) error {
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	result, err := json.Marshal(&src)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(result, dst); err != nil {
		return err
	}

	return nil
}

//Remove special characters from number values
func FormatNumber(value string, numberType string) string {
	fv := strings.Trim(strings.ReplaceAll(strings.ReplaceAll(value, "$", ""), ",", ""), " ")
	fv = strings.ReplaceAll(fv, "%", "")
	if numberType == "int" {
		_, err := strconv.ParseInt(fv, 10, 64)
		if err != nil {
			fv = "0"
		}
	} else {
		_, err := strconv.ParseFloat(fv, 64)
		if err != nil {
			fv = "0.0"
		}
	}

	return fv
}

//ParseDate parses a date based on the following known formats:
// DD/MM/YYYY, DD-MM-YYYY, YYYY-MM-DD, D/M/YYYY, D-M-YYYY, YYYY-M-D
// YYYY/M/D
func ParseDate(date string) *time.Time {
	dateLength := 8
	if len(date) == 10 {
		dateLength = 8
	}

	for _, format := range dateFormats[dateLength] {
		formattedDate, err := time.Parse(format, date)
		if err == nil {
			return &formattedDate
		}
	}

	return nil
}

var dateFormats map[int][]string = map[int][]string{
	10: {
		"02/01/2006",
		"02-01-2006",
		"2006-01-02",
		"2006/01/02",
	},
	8: {
		"2/1/2006",
		"2/1/2006",
		"2/01/2006",
		"2-1-2006",
		"2-01-2006",
		"02-1-2006",
		"2006-1-2",
		"2006-01-2",
		"2006-1-02",
		"2006/1/2",
		"2006/01/2",
		"2006/1/02",
	},
}
