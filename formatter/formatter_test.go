package formatter

import (
	"fmt"
	"testing"
	"time"
)

var tests = []struct {
	Input       string
	InputFormat string
	Expected    func(string, string) string
}{
	{
		"02/01/2009",
		"02/01/2006",
		parseExpectedDate,
	},
	{
		"2/1/2009",
		"2/1/2006",
		parseExpectedDate,
	},
	{
		"2/01/2009",
		"2/01/2006",
		parseExpectedDate,
	},
	{
		"02/1/2009",
		"02/1/2006",
		parseExpectedDate,
	},
	{
		"2009/01/02",
		"2006/01/02",
		parseExpectedDate,
	},
	{
		"2009/1/02",
		"2006/1/02",
		parseExpectedDate,
	},
	{
		"2009/01/2",
		"2006/01/2",
		parseExpectedDate,
	},
	{
		"2009-01-02",
		"2006-01-02",
		parseExpectedDate,
	},
	{
		"2009-1-02",
		"2006-1-02",
		parseExpectedDate,
	},
	{
		"2009-01-2",
		"2006-01-2",
		parseExpectedDate,
	},
}

func TestDateFormatter(t *testing.T) {
	for _, test := range tests {
		input := ParseDate(test.Input).Format("2006-01-02")
		expected := test.Expected(test.Input, test.InputFormat)
		if input != expected {
			fmt.Printf("test failed: expected %s got %s", input, expected)
			t.Fail()
		}
	}
}

func parseExpectedDate(date, format string) string {
	t, err := time.Parse(format, date)
	if err != nil {
		d := time.Now()
		return d.Format("2006-01-02")
	}
	return t.Format("2006-01-02")
}
